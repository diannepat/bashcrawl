#!/bin/bash
#
# If you are reading this, you have wandered out of bounds
# and are reading the code that drives the game.
#
#                    Congratulations!
#
# Learning Linux is all about curiosity, so read this code and see
# if you can figure out what it does.
#
# Any line, like this one, that starts with a hash_mark is a comment.
# Comments are here to help explain the code to you.

#-----------------------------------------------------------------
job1 ()
{
clear
echo "job1: build the storerooms"
if [ ! -d buttery ] || [ ! -d larder ] || [ ! -d pantry ]; then
cat << EOF  
  Build the larder (for meats), pantry (for breads), and buttery (for spirits).

  Run ./master_build from the entrance for "build" tips.
EOF
  elif [ -d buttery ] && [ -d larder ] && [ -d pantry ]; then
  echo
  echo "Congratulations, you have already built all the storerooms."
  exit 1
fi
}

#-----------------------------------------------------------------
job2 ()
{
clear
echo "job2: link the storerooms to the kitchen"
if [ ! -L kitchen/buttery ] || [ ! -L kitchen/larder ] || [ ! -L kitchen/pantry ]; then 
  echo "Run ./master_build from the entrance for "link" tips."
elif [ -L kitchen/buttery ] && [ -L kitchen/larder ] && [ -L kitchen/pantry ]; then
  echo
  echo "Congratulations, you have already linked all the storerooms to the kitchen with magic portals."
  exit 1
fi
}

#-----------------------------------------------------------------
job3 () 
{
clear
echo "job3: dig the root_cellar"
if [ ! -d kitchen/root_cellar ]; then
  echo "Run ./master_build from the entrance for "dig" tips."
elif [ -d kitchen/root_cellar ]; then
  echo
  echo "Congratulations, you have already dug the root_cellar."
  exit 1
fi
}

#-----------------------------------------------------------------
job4 () {
clear
echo "job4: banish bad foodstuffs from the food_shack"
echo " Remove stale, spoiled, maggoty, rotten, and sour items."

echo "___________________________________"

bad_food_count=$(find food_shack/*{stale,spoiled,maggoty,rotten,sour} | wc -w)

if [ $bad_food_count -gt 0 ]; then
  echo "Yuck, I found this bad food in the food_shack:"
  echo
  for f in $(find food_shack/*{stale,spoiled,maggoty,rotten,sour})
    do [ -f $f ] || continue 
    food=$(basename $f)
    echo "$food"
  done
  echo
  echo "Run ./master_banishment from the entrance for tips."
  echo
else
  echo 
  echo "Congratulations, you have already cleaned the bad foodstuffs from the food_shack!"
  echo
fi

}

#-----------------------------------------------------------------
job5 () {
clear
echo "job5: Move foodstuffs out of the nasty food_shack into the new storerooms" 

foodshack_count=$(ls -A food_shack | wc -w)

echo "food_shack contains ${foodshack_count} items."
echo
if [ $foodshack_count -gt 0 ]; then
cat << EOF 
Move ALL items from the nasty food_shack:
  - drinks to the buttery, 
  - bread to the pantry, 
  - meat to the larder, 
  - root vegetables to the root_cellar.

Remove anything else that might be left in the food_shack, even if it is hidden!

Run ./master_move from the entrance for tips.
EOF
fi

echo
echo "Checking count of items in each store room:"
buttery_count=$(ls buttery | wc -w)
larder_count=$(ls larder | wc -w)
pantry_count=$(ls pantry | wc -w)
root_cellar_count=$(ls kitchen/root_cellar | wc -w)

if [ ${buttery_count} -ne 13 ]; then echo "buttery contents incorrect"; else echo "buttery contents correct!"; fi
if [ ${larder_count} -ne 11 ]; then echo "larder contents incorrect"; else echo "larder contents correct!"; fi
if [ ${pantry_count} -ne 22 ]; then echo "pantry contents incorrect"; else echo "pantry contents correct!"; fi
if [ ${root_cellar_count} -ne 26 ]; then echo "root_cellar contents incorrect"; else echo "root_cellar contents correct!"; fi

}

#-----------------------------------------------------------------
job6 () {
clear
echo "job6: Demolish the food_shack"
if [ -d food_shack ]; then
  echo "Run ./master_demolition from the entrance for tips."
  else echo "Congratulations, the food_shack has been removed!"
fi

}

#-----------------------------------------------------------------
job7 () {
clear
cat << EOF 
job7: Tally the foodstuffs!

First, go to the kitchen!

Tally the foodstuffs from each storeroom in a text file called tally_scroll:

List how many of each type of bread are in the pantry.
List how many of each spirit is in the buttery.
List how many of each meat is in the larder.
List how many of each vegetable is in the root_cellar.

The tally_scroll will have entries like this:

pantry:horse_bread 3
pantry:wastel_bread 8

_______________________

You need to create the tally_scroll. From the kitchen use touch to create the tally_scroll like this:

touch ../tally_scroll

One nice thing about touch, is that it won't hurt anything if the tally_scroll already exists.

________________________

COUNTING
Try the 'wc -w' (word count) spell, like this:

ls -L buttery/mead* | wc -w

________________________

EOF

if [ -f ../tally_scroll ]; then
echo "This is the current tally_scroll:"
echo 
cat ../tally_scroll
else
echo
echo "There is no tally_scroll! Are you sure you are in the kitchen?"
echo
fi

echo
echo "-----------------------"
echo "SCRIBING"
echo "Try this from the kitchen, to generate a nice entry for the tally:"
echo 'echo "pantry:horse_bread": $(ls -L pantry/*horse_bread* | wc -w)'
echo
echo "If you like it, you can append it into the tally_scroll in the entrance (one level up):"
echo
echo 'echo "pantry:horse_bread": $(ls -L pantry/*horse_bread* | wc -w) >>../tally_scroll'
echo
echo "Look at the results:"
echo
echo "cat ../tally_scroll"
echo
echo "Run ./master_tally from the entrance for tips."

echo "-----------------------"



}

# --------------------------------------------------------------------
# If you already received the jacket, it is in your inventory
# 
if grep  --quiet --only-matching jacket <<< $I; then
  echo "Congratulations, your master mason guild jacket has already been accepted."
  exit 1
fi

if ! grep  --quiet --only-matching bonus <<< $I; then
cat << EOF 

There are seven jobs. Indicate which job (e.g., job1) you wish to try.
Keep your wits about you and always know where you are and what surrounds you.
____________

  job1
    Build these storerooms: larder (for meats), pantry (for breads), and buttery (for spirits). 
  _____________

  job2 
    Link the kitchen to each storage room directly with magic portals.
  _____________

  job3 
    Dig a root_cellar under the kitchen.
  _____________

  job4 
    Banish the bad foodstuffs: stale, spoiled, maggoty, rotten, and sour items.
  _____________

  job5 
    Move good drinks to the buttery, bread to the pantry, meat to the larder, and root vegetables to the root_cellar.
  _____________

  job6
    Remove the food_shack 
  _____________

  job7
   Tally the good foodstuffs
____________

If you have run all the jobs successfully, it is time for the inspection.

EOF
# Wait for user to enter response
read RESP

  if [ "$RESP" = "job1" ];then
    job1
  elif [ "$RESP" = "job2" ];then
    job2
  elif [ "$RESP" = "job3" ];then
    job3
  elif [ "$RESP" = "job4" ];then
    job4
  elif [ "$RESP" = "job5" ];then
    job5
  elif [ "$RESP" = "job6" ];then
    job6
  elif [ "$RESP" = "job7" ];then
    job7
 fi
fi
