

_____________

SCROLL OF CONFIGURATION

Illusions are strong here.
It is difficult to tell what is a doorway to another room and what is an object.
But the `ls` command is many-colored.  

In the Google Cloud Shell:

- Directories (the rooms of this castle) are blue (like the armoury)
- Programs are green (like the treasure).
- Scrolls are white.

Be warned! other systems of unix may use different colors! 
Clear the illusions by marking file type:

ls -F

No matter the colors that swirl around you, 
- `ls -F` marks the directories with an appended slash `/`, 
- Programs have an appended star `*`
- Plain text files, like the scroll, lack any special mark.  

armoury/  scroll  treasure*

_____________

INVOKE A PROGRAM

You can interact with an item (program) in the room 
by running it as a command (a shell script).

For example, collect treasure like this:

./treasure
 
The . (dot) means THE CURRENT DIRECTORY, and the name of the 
script makes the code run.  

The ./ is known as a relative path,
and the . means the current room (directory).
 
_____________

ALIASES

Avoid typing `ls -F` every time by running the
following:

alias lf='ls -F'

This is known as a shell or command alias.  With this alias,
simply typing `lf` will run `ls -F`. Try it out!

lf

_____________

SEARCH PATH

The search path is a special environment variable that tells
bash where to look for executable programs.

To learn about the treasure, you had to prepend `./` 
to treasure so that bash would understand 
you meant the program in THE CURRENT DIRECTORY. 

This is because the current directory is not in the search path!

Instead, cast a spell to add dot to the search path, so bash 
will ALWAYS look for programs in THE CURRENT DIRECTORY first, even
if you don't prepend the .

export PATH=.:$PATH

Now you can type the name of the program without the ./  
For example:

    treasure

instead of 

    ./treasure

Try it now! 

_____________

TAB COMPLETION

In fact, you can just type tr and hit tab! Now bash knows to look for 
commands in the current directory and because treasure is recognized 
as a command in your path, bash will guess that t followed by a tab
might refer to treasure!

_____________

ENVIRONMENT VARIABLES

You have added the amulet to your inventory! 
The inventory is an environment variable and you can ensure you append values 
(instead of replacing them) by adding `,$I` without any spaces!, like this:

export I=amulet,$I

_____________

FUNCTIONS

Make a function to ease adding stuff to your inventory.  
This function will add to your inventory, and tell you what you have:

get () { export I=$1,$I ; echo $I; }

_____________

RESET ENVIRONMENT VARIABLE

To try it, first clear your inventory by unsetting the environment variable:

unset I

_____________

TRY THE FUNCTION

Now try your new function:

get amulet

This will add the amulet and list the current contents of the inventory!

___________

THE TEMPORARY NATURE OF THINGS

If your Cloud shell session times out, or you have to start a new terminal window
or come back another day, your magic spells for alias, search path, and function 
will disappear like dust in the wind.

The solution is to write them down in a book of spells called a configuration file.
Fortunately, a hidden configuration file already exists at the entrance. 

_____________

CONFIGURATION FILE

You can remind bash of all these handy spells at any time with the incantation that tells
bash to read the configuration file.  You must tell bash where the configuration file is.

For example, because you are in the cellar, it may be easiest to provide the relative 
path to the configuration file:

source ../.bcrawl

If you'd like to read it, use `cat`, just like you do for the scrolls:

cat ../.bcrawl

_____________

THE FUTURE

In the future when you return to bashcrawl, you can source the configuration 
file at the entrance.

Don't worry, the scroll at the entrance has now been updated to remind you to 

source .bcrawl

____________

Remember, you can peek into rooms below with your map:

tree

_____________


END OF YE OLD SCROLL OF CONFIGURATION


______________
